import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";

export default function Footer() {
  return (
    <Grid item xs={4} style={{ textAlign: "center" }}>
      <div>Rights Reserved.</div>
      <Box sx={{ mb: 2 }}></Box>
    </Grid>
  );
}
