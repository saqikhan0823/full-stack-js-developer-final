import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import AdbIcon from "@mui/icons-material/Adb";
import styled from "@mui/system";
import { createTheme } from "@mui/material/styles";
import Image from "next/image";

const pages = [
  { page: "Shop", href: "/" },
  { page: "Recipes", href: "/recipes" },
  { page: "Learn", href: "/learn" },
  { page: "About", href: "/about" },
  { page: "Blog", href: "/blog" },
];


export default function NavBar() {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <AppBar
      className="navColor"
      position="static"
      sx={{ bgcolor: "white", boxShadow: "none" }}
    >
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Box sx={{ml:9}}></Box>
        <Image src="/images/logo.png" width={100} height={105} 
          
        />

          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" }}}>
          
            {pages.map((page) => (
              <Button
                key={page.href}
                onClick={handleCloseNavMenu}
                href={page.href}
                sx={{
                  color: "black",
                  display: "block",
                  fontSize: "1.2rem",
                }}
              >
                {page.page}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
