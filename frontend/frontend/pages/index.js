import Head from "next/head";
import styles from "../styles/Home.module.css";
import Shop from "./shop";


export default function Home() {
  return <Shop />;
}
