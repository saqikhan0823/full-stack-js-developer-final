import * as React from "react";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import Stack from "@mui/material/Stack";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Image from "next/image";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import { styled } from "@mui/system";
import StopCircleIcon from "@mui/icons-material/StopCircle";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import PrintIcon from "@mui/icons-material/Print";

function handleClick(event) {
  event.preventDefault();
  console.info("You clicked a breadcrumb.");
}

const Duration = styled("div")({
  padding: "10px 0px",
  display: "flex",
});

const CustomLabel = styled("div")({
  fontWeight: "bold",
});

export default function Blog() {
  const breadcrumbs = [
    <Link
      underline="hover"
      key="1"
      color="inherit"
      href="#"
      onClick={handleClick}
    >
      Recipes
    </Link>,
    <Link
      underline="hover"
      key="2"
      color="inherit"
      href="#"
      onClick={handleClick}
    >
      Nachos
    </Link>,
    <Typography key="3" color="text.primary">
      Spicy Nachos
    </Typography>,
  ];

  return (
    <Container maxWidth="xl" sx={{ ml: 17 }}>
      <Box
        sx={{
          width: 25,
          height: 10,
        }}
      ></Box>
      <Grid container spacing={2}>
        <Grid md={5}>
          <Stack spacing={2} sx={{ mt: 2 }}>
            <Breadcrumbs
              separator="›"
              aria-label="breadcrumb"
              sx={{ fontWeight: "bold", color: "black" }}
            >
              {breadcrumbs}
            </Breadcrumbs>
          </Stack>
          <Typography variant="h3" gutterBottom>
            Cheese Dipped Crispy Nachos
          </Typography>
          <Box
            sx={{
              width: 250,
              height: 100,
            }}
          ></Box>
          <Typography
            variant="body1"
            gutterBottom
            sx={{ fontSize: "1.2rem", align: "justify" }}
          >
            Nachos have always been my “I’m too tired to cook after grocery
            shopping” dinner. We’ve all been there, am I right? You’ve spent a
            fair amount of your paycheck on groceries, but you’re so tired you
            need a quick meal to throw together. Nachos have it all: protein,
            carbs, veggies, and—the most important thing—cheese! They’re also
            fully customizable to suit a solo diner or a group gathered for
            chow.
          </Typography>
        </Grid>
        <Grid md={7}>
          <Box sx={{ ml: 5, mt: 5 }}>
            <Image src="/images/nachos.jpg" width={600} height={500} />
          </Box>
        </Grid>
        <Grid md={5}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              bgcolor: "background.paper",
              fontSize: "1.3rem",
            }}
          >
            <Duration>
              <div>
                <AccessTimeIcon sx={{ fontSize: "50px" }} />
              </div>
              <Box sx={{ ml: 2 }}></Box>
              <div>
                <Typography variant="body1">PREP</Typography>
                <CustomLabel>10 mins</CustomLabel>
              </div>
            </Duration>
            <Box sx={{ ml: 5 }}></Box>
            <Duration>
              <div>
                <Typography variant="body1">Bake</Typography>
                <CustomLabel>1 hr to 1 15 mins</CustomLabel>
              </div>
              <Box sx={{ ml: 5 }}></Box>
              <div>
                <Typography variant="body1">Total</Typography>
                <CustomLabel>1 hr 10 mins</CustomLabel>
              </div>
            </Duration>
          </Box>
          <Box sx={{ mb: 3 }}></Box>
          <Divider variant="middle" />
          <Box sx={{ mt: 3 }}></Box>
        </Grid>

        <Grid md={8}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              bgcolor: "background.paper",
              fontSize: "1.3rem",
            }}
          >
            <Duration>
              <div>
                <StopCircleIcon sx={{ fontSize: "50px" }} />
              </div>
              <Box sx={{ ml: 2 }}></Box>
              <div>
                <Typography variant="body1">Yield</Typography>
                <CustomLabel>1 Loaf, 12 servings</CustomLabel>
              </div>
            </Duration>
            <Box sx={{ ml: 5 }}></Box>
            <Duration>
              <div>
                <Button
                  variant="outlined"
                  startIcon={<AddIcon />}
                  sx={{
                    borderColor: "red",
                    color: "black",
                  }}
                >
                  Save Recipe
                </Button>
              </div>
              <Box sx={{ ml: 5 }}></Box>
              <div>
                <Button
                  variant="outlined"
                  startIcon={<PrintIcon />}
                  sx={{
                    borderColor: "red",
                    color: "black",
                  }}
                >
                  Print
                </Button>
              </div>
            </Duration>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}
