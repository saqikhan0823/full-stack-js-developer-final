import '../styles/globals.css';
import NavBar from "../components/navbar";
import NavBar2 from "../components/navbar2";
import Grid from '@mui/material/Grid';
import Footer from '../components/footer'


export default function App({ Component, pageProps }) {
    return <>
    <div><NavBar/></div>
    <div><NavBar2/></div>
    <Component {...pageProps} />
    <div><Footer/></div>
    </>
  }


  