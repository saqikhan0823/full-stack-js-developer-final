const axios = require('axios');
const Servers = [
    {
        url: "TestMy.net",
        priority: 1
    },
    {
        url: "https://gitlab.com",
        priority: 4
    },
    {
        url: "http://app.scnt.me",
        priority: 3
    },
    {
        url: "https://www.google.com",
        priority: 2
    }
];

class Server {
    servers;
    constructor() {
        this.servers = Servers;
    }
    /** 
     * @params :  {}
     * @Description : This method is use to find the online servers.
     * */
    async findServer() {
        try {
            const requests = this.servers.map((serverDetail) => {
                return this.connectToServer(serverDetail);
            });
            console.log(requests);
            const serverResp = await Promise.all(requests);
            const activeServers = serverResp.filter(server => server.status >= 200 && server.status <= 299)
            console.log("activeServers");
            const availableServer = activeServers.reduce((acc, item) => (acc?.priority > item.priority) ? acc : item, null)
            return availableServer ? availableServer : { status: 404, message: 'No server found' };
        } catch (error) {
            console.log(error);
            throw error;
        }
    }
    
    
    
    /**
     * @param  {IServerDetails} serverDetail
     * @Description : This method is use to return axios promise.
     */
    connectToServer(serverDetail) {
        const config = {
            url: serverDetail.url,
            method: 'get',
            timeout: 5000
        }
        return axios(config)
        .then(response => {
            return { statusCode: response.status, priority: serverDetail.priority, url: serverDetail.url }
        })
        .catch(err => {
            return { statusCode: err.errno, priority: serverDetail.priority, url: serverDetail.url }
        })
    }
}

module.exports = new Server();
