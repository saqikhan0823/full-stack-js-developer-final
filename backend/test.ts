const axios = require('axios');
const servers = require("./server.ts");
const serverDetails = [
  {
      url: "TestMy.net",
      priority: 1
  },
  {
      url: "https://gitlab.com",
      priority: 4
  },
  {
      url: "http://app.scnt.me",
      priority: 3
  },
  {
      url: "https://www.google.com",
      priority: 2
  }
];

jest.mock('axios');

describe('Testing job', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('Is Server online', async () => {
    axios.get.mockResolvedValue({
      status: 200,
    });
    const server = await servers.findServer();
    expect(server.statusCode).toEqual(200);
    expect(server.priority).toEqual(serverDetails[0].priority);
    expect(server.url).toEqual(serverDetails[0].url);
  });

  it('Promise for fetching data', async () => {
    axios.get.mockResolvedValue({
      status: 500,
    });
    const server = await servers.connectToServer(serverDetails[0]);
    expect(server.statusCode).toEqual(500);
    expect(server.priority).toEqual(serverDetails[0].priority);
    expect(server.url).toEqual(serverDetails[0].url);
  });

  it('No servers are online', async () => {
    axios.get.mockResolvedValue({ errno: 404 });
    try {
      await servers.findServer();
    } catch (error) {
      expect(error.message).toEqual('No servers are online');
    }
  });
});
