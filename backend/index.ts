const axios = require('axios');
const servers = require("./server.ts");
const http = require("http");

const HOST = "localhost";
const PORT = 3000;


const requestListener = async function (req, res) {
  if (req.method.toLowerCase() === 'get') {
    try {
      const response = await servers.findServer();
      res.writeHead(200);
      return res.end(JSON.stringify(response));
    } catch (error) {
      res.writeHead(404);
      return res.end(JSON.stringify({ msg: error.message }));
    }
  }
  res.writeHead(400);
  return res.end(JSON.stringify({ msg: 'Invalid Request Created' }));
};

const server = http.createServer(requestListener);
server.listen(PORT, HOST, () => {
  console.log(`Server is running on http://${HOST}:${PORT}`);
});
